const serverless = require("serverless-http");
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const users = require("./data/user.json");
const app = express();

app.use(cors());
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.get("/user", function (req, res) {
  return res.json(users);
});

app.post("/user", function (req, res) {

  if (req.body.firstName != undefined){
    const id = "user"+ (users.length + 1);
    const name = req.body.firstName;
    const lastname = req.body.lastName;
    const location = req.body.location;
    const user = { id, name, lastname, location };
    users.push(user);
    return res.json(user);
  }else{
    return res.send("No hay parametros");
  }
});


// app.listen(8080, function () {
//   console.log("Servidor iniciado");
// });
module.exports.handler = serverless(app);
