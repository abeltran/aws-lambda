# LAMBDA AWS

# Instalaciones requeridas

- NodeJs 12.x
- Serverless

________________________________________________________________________

# Node
https://nodejs.org/en/


_________________________________________________________________________

# Iniciando proyecto

- Clonar repositorio
- Instalar serverless
- Instalar dependencias locales
- Test en localhost

Es totalmente necesario instalar nodejs para poder instalar serverless

- `npm i serverless -g`

Para instalar las dependencias locales ejecutar

- `npm install`

Para hacer pruebas en local ejecutar:

- `node main.js `
- Ir a localhost:8080/user 

_________________________________________________________________________

# Configurar AWS con serverless

Para administrar las funciones lambda con **serverless** necesitamos un usuario nuevo dentro de IAM con los siguientes permisos a asignar :

- AWSLambdaFullAccess
- IAMFullAccess
- AmazonAPIGatewayAdministrator
- AWSCloudFormationFullAccess

Una vez que generaron el usuario dentro de IAM les dara una key y una secretkey (resguardenlos) porque no se podrá ver de nuevo la secret key

Despues ejecutar en consola dentro de la raiz del proyecto y colocar las llaves del usuario creado

`serverless config credentials --provider aws --key XXXXXXXXXXX --secret XXXXXxxxxXXXXXxxKEY`

_________________________________________________________________________

# LAMBDA

Para ejecutar el deploy basta con comentar dentro de main.js la función `app.listen(....)` y descomentar la siguiente línea: `module.exports.handler = serverless(app)`

El comando para ejecutar es: 

`serverless deploy`






